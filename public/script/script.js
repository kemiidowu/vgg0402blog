// to display post on blog page

function displayPost() {
  function loading(){
    let loading= "Loading... "
    document.querySelector("#postedList").innerHTML = loading;
  }
  loading()
  fetch("https://jsonplaceholder.typicode.com/posts")
    .then(response => response.json())
    .then(data => {
      console.log(data);

      // To paginate
      (function() {
        document.getElementById("first").addEventListener("click", firstPage);
        document.getElementById("next").addEventListener("click", nextPage);
        document
          .getElementById("previous")
          .addEventListener("click", previousPage);
        document.getElementById("last").addEventListener("click", lastPage);

        var list = data;
        var pageList = new Array();
        var currentPage = 1;
        var numberPerPage =10;
        var numberOfPages = 0;

        function makeList() {
          numberOfPages = getNumberOfPages();
        }

        function getNumberOfPages() {
          return Math.ceil(list.length / numberPerPage);
        }
        function nextPage() {
          currentPage += 1;
          loadList();
        }
        function previousPage() {
          currentPage -= 1;
          loadList();
        }
        function firstPage() {
          currentPage = 1;
          loadList();
        }
        function lastPage() {
          currentPage = numberOfPages;
          loadList();
        }
        function loadList() {
          var begin = (currentPage - 1) * numberPerPage;
          var end = begin + numberPerPage;
          pageList = list.slice(begin, end);

          drawList();
          check();
        }

        function drawList() {
          if (pageList) {
            console.log(pageList);
            let list = "";

            for (let i = 0; i < pageList.length; i++) {
              let currentPost = pageList[i];

              list += `<div class="col-md-6 col-sm-6">
        <div class="single-blog-item">
            <div class="single-blog-item-img">
                <img src="/public/image/b1.jpg" alt="blog image">
            </div>
            <div class="single-blog-item-txt">
                <h2><a href="blogView.html?id=${currentPost.id}"> ${currentPost.title} </a></h2>
                   
                <h4>posted <span>by</span> <a href="#">admin</a> Oct 2019</h4>
                <p> ${currentPost.body} </p>
            </div>
        </div>
    </div>`;

              document.querySelector("#postedList").innerHTML = list;
            }
          } else {
            document.querySelector(
              "#postedList"
            ).innerHTML = `<div class="col-md-6 col-sm-6">
    <div class="single-blog-item">
        <div class="single-blog-item-txt">
            <h2 class="text-danger">An error occured</h2>
            </div>
        </div>
    </div>`; // false
          }
        }
        function check() {
          document.getElementById("next").disabled =
            currentPage == numberOfPages ? true : false;
          document.getElementById("previous").disabled =
            currentPage == 1 ? true : false;
          document.getElementById("first").disabled =
            currentPage == 1 ? true : false;
          document.getElementById("last").disabled =
            currentPage == numberOfPages ? true : false;
        }

        function load() {
          makeList();
          loadList();
        }

        window.onload = load();
      })();
    });
}
displayPost();

// To display comment page in relative to id

let queryParameters = location.search;
let sanitizedQP = queryParameters.substring(1);

let sanitizedQPArray = sanitizedQP.split("&");

params = {};

sanitizedQPArray.forEach(function(sanitizedQP) {
  paramsKeyValueArray = sanitizedQP.split("=");
  params[paramsKeyValueArray[0]] = paramsKeyValueArray[1];
});
console.log(params);

readPostAndComments(params.id);

function readPostAndComments(id) {
  fetch(`https://jsonplaceholder.typicode.com/posts/${id}`)
    .then(response => response.json())
    .then(data => {
      console.log(data);

      let list = "";

      list += `
          <h1>
              ${data.title}
          </h1>
      
      <div class="content-wrap">
          <p>
              ${data.body}
          </p>

        <p>
          Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.
          </p>

          <p>
          Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.
          </p>									
      </div>`;

      document.querySelector(".output").innerHTML = list;
    });

  fetch(
    `https://jsonplaceholder.typicode.com/posts/${id}/comments?postId=${id}`
  )
    .then(response => response.json())
    .then(data => {
      console.log(data);
      if (data) {
        let list = "";

        for (let i = 0; i < data.length; i++) {
          let currentPost = data[i];

          list += `<div class="comment-list left-padding">
          <div class="single-comment justify-content-between d-flex">
              <div class="user justify-content-between d-flex">
                
                  <div class="desc">
                      <h5>${currentPost.name}</h5>
                      <p>${currentPost.email}</p>
                      <p class="date">December 4, 2017 at 3:12 pm </p>
                      <p class="comment">
                      ${currentPost.body}
                      </p>
                  </div>
              </div>
              <div class="reply-btn">
                      <a href="#commentForm" class="btn-reply text-uppercase">reply</a> 
              </div>
          </div>
      </div>`;

          document.querySelector(
            ".outputComment"
          ).innerHTML = `<h5 class="text-uppercase pb-80">${data.length} Comments</h5>${list}`;
        }
      } else {
        return undefined; // false
      }
    });
}

// Sticky header

var navBar = document.getElementById("myHeader");
var header = document.getElementById("myHeader");
var sticky = header.offsetTop;

function myFunction() {
  if (window.pageYOffset > sticky) {
    header.classList.add("sticky");
    navBar.classList.remove("fixed");
  } else {
    header.classList.remove("sticky");
  }
}
window.onscroll = function() {
  myFunction();
};

// function(){
//   document.getElementById("first").addEventListener("click", firstPage);
//   document.getElementById("next").addEventListener("click", nextPage);
//   document
//         .getElementById("previous")
//         .addEventListener("click", previousPage);
//       document.getElementById("last").addEventListener("click", lastPage);
//       var list = JSON();
//   var pageList = new Array();
//   var currentPage = 1;
//   var numberPerPage = 10;
//   var numberOfPages = 0;
// function makeList() {
//   for (x = 0; x < 200; x++)
//       list.push(x);
//   numberOfPages = getNumberOfPages();
// }

// function getNumberOfPages() {
//   return Math.ceil(list.length / numberPerPage);
// }
// function nextPage() {
//   currentPage += 1;
//   loadList();
// }
// function previousPage() {
//   currentPage -= 1;
//   loadList();
// }
// function firstPage() {
//   currentPage = 1;
//   loadList();
// }
// function lastPage() {
//   currentPage = numberOfPages;
//   loadList();
// }
// function loadList() {
//   var begin = ((currentPage - 1) * numberPerPage);
//   var end = begin + numberPerPage;
//   pageList = list.slice(begin, end);
//   drawList();
//   check();
// }

// function drawList() {
//   document.getElementById("list").innerHTML = "";
//   for (r = 0; r < pageList.length; r++) {
//       document.getElementById("list").innerHTML += pageList[r] + "<br/>";
//   }
// }
// function check() {
//   document.getElementById("next").disabled = currentPage == numberOfPages ? true : false;
//   document.getElementById("previous").disabled = currentPage == 1 ? true : false;
//   document.getElementById("first").disabled = currentPage == 1 ? true : false;
//   document.getElementById("last").disabled = currentPage == numberOfPages ? true : false;
// }
// function load() {
//   makeList();
//   loadList();
// }

// window.onload = load;
